-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product` ;

CREATE TABLE IF NOT EXISTS `product` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CATEGORY` ;

CREATE TABLE IF NOT EXISTS `CATEGORY` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SUPPLIER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SUPPLIER` ;

CREATE TABLE IF NOT EXISTS `SUPPLIER` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `companyname` VARCHAR(255) NOT NULL,
  `contactname` VARCHAR(255) NOT NULL,
  `address` TINYTEXT NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `postalcode` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PRODUCT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PRODUCT` ;

CREATE TABLE IF NOT EXISTS `PRODUCT` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `category` INT NOT NULL,
  `supplier` INT NOT NULL,
  `unit` VARCHAR(255) NOT NULL,
  `price` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_PRODUCT_CATEGORY`
    FOREIGN KEY (`category`)
    REFERENCES `CATEGORY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRODUCT_SUPPLIER1`
    FOREIGN KEY (`supplier`)
    REFERENCES `SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_PRODUCT_CATEGORY_idx` ON `PRODUCT` (`category` ASC) VISIBLE;

CREATE INDEX `fk_PRODUCT_SUPPLIER1_idx` ON `PRODUCT` (`supplier` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `CATEGORY`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (1, 'Beverages', 'Soft drinks, coffees, teas, beers, and ales');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (2, 'Condiments', 'Sweet and savory sauces, relishes, spreads, and seasonings');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (3, 'Confections', 'Desserts, candies, and sweet breads');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (4, 'Dairy Products', 'Cheeses');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (5, 'Grains/Cereals', 'Breads, crackers, pasta, and cereal');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (6, 'Meat/Poultry', 'Prepared meats');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (7, 'Produce', 'Dried fruit and bean curd');
INSERT INTO `CATEGORY` (`id`, `name`, `description`) VALUES (8, 'Seafood', 'Seaweed and fish');

COMMIT;


-- -----------------------------------------------------
-- Data for table `SUPPLIER`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (1, 'Exotic Liquid', 'Charlotte Cooper', '49 Gilbert St.', 'Londona', 'EC1 4SD', 'UK', '(171) 555-2222');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (2, 'New Orleans Cajun Delights', 'Shelley Burke', 'P.O. Box 78934', 'New Orleans', '70117', 'USA', '(100) 555-4822');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (3, 'Grandma Kelly\\\'s Homestead', 'Regina Murphy', '707 Oxford Rd.', 'Ann Arbor', '48104', 'USA', '(313) 555-5735');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (4, 'Tokyo Traders', 'Yoshi Nagase', '9-8 Sekimai Musashino-shi', 'Tokyo', '100', 'Japan', '(03) 3555-5011');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (5, 'Cooperativa de Quesos \\\'Las Cabras\\\'', 'Antonio del Valle Saavedra ', 'Calle del Rosal 4', 'Oviedo', '33007', 'Spain', '(98) 598 76 54');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (6, 'Mayumi\\\'s', 'Mayumi Ohno', '92 Setsuko Chuo-ku', 'Osaka', '545', 'Japan', '(06) 431-7877');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (7, 'Pavlova, Ltd.', 'Ian Devling', '74 Rose St. Moonie Ponds', 'Melbourne', '3058', 'Australia', '(03) 444-2343');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (8, 'Specialty Biscuits, Ltd.', 'Peter Wilson', '29 King\\\'s Way', 'Manchester', 'M14 GSD', 'UK', '(161) 555-4448');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (9, 'PB KnÃ¤ckebrÃ¶d AB', 'Lars Peterson', 'Kaloadagatan 13', 'GÃ¶teborg', 'S-345 67', 'Sweden ', '031-987 65 43');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (10, 'Refrescos Americanas LTDA', 'Carlos Diaz', 'Av. das Americanas 12.890', 'SÃ£o Paulo', '5442', 'Brazil', '(11) 555 4640');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (11, 'Heli SÃ¼ÃŸwaren GmbH &amp; Co. KG', 'Petra Winkler', 'TiergartenstraÃŸe5', 'Berlin', '10785', 'Germany', '(010) 9984510');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (12, 'Plutzer LebensmittelgroÃŸmÃ¤rkte AG', 'Martin Bein', 'Bogenallee 51', 'Frankfurt', '60439', 'Germany', '(069) 992755');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (13, 'Nord-Ost-Fisch Handelsgesellschaft mbH', 'Sven Petersen', 'Frahmredder 112a', 'Cuxhaven', '27478', 'Germany', '(04721) 8713');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (14, 'Formaggi Fortini s.r.l.', 'Elio Rossi', 'Viale Dante, 75', 'Ravenna', '48100', 'Italy', '(0544) 60323');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (15, 'Norske Meierier', 'Beate Vileid', 'Hatlevegen 5', 'Sandvika', '1320', 'Norway', '(0)2-953010');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (16, 'Bigfoot Breweries', 'Cheryl Saylor', '3400 - 8th Avenue Suite 210', 'Bend', '97101', 'USA', '(503) 555-9931');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (17, 'Svensk SjÃ¶fÃ¶da AB', 'Michael BjÃ¶rn', 'BrovallavÃ¤gen 231', 'Stockholm', 'S-123 45', 'Sweden', '08-123 45 67');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (18, 'Aux joyeux ecclÃ©siastiques', 'GuylÃ¨ne Nodier', '203, Rue des Francs-Bourgeois', 'Paris', '75004', 'France', '(1) 03.83.00.68');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (19, 'New England Seafood Cannery', 'Robb Merchant', 'Order Processing Dept. 2100 Paul Revere Blvd.', 'Boston', '02134', 'USA', '(617) 555-3267');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (20, 'Leka Trading', 'Chandra Leka', '471 Serangoon Loop, Suite #402', 'Singapore', '0512', 'Singapore', '555-8787');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (21, 'Lyngbysild', 'Niels Petersen', 'Lyngbysild Fiskebakken 10', 'Lyngby', '2800', 'Denmark', '43844108');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (22, 'Zaanse Snoepfabriek', 'Dirk Luchte', 'Verkoop Rijnweg 22', 'Zaandam', '9999 ZZ', 'Netherlands', '(12345) 1212');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (23, 'Karkki Oy', 'Anne Heikkonen', 'Valtakatu 12', 'Lappeenranta', '53120', 'Finland', '(953) 10956');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (24, 'G\\\'day, Mate', 'Wendy Mackenzie', '170 Prince Edward Parade Hunter\\\'s Hill', 'Sydney', '2042', 'Australia', '(02) 555-5914');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (25, 'Ma Maison', 'Jean-Guy Lauzon', '2960 Rue St. Laurent', 'MontrÃ©al', 'H1J 1C3', 'Canada', '(514) 555-9022');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (26, 'Pasta Buttini s.r.l.', 'Giovanni Giudici', 'Via dei Gelsomini, 153', 'Salerno', '84100', 'Italy', '(089) 6547665');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (27, 'Escargots Nouveaux', 'Marie Delamare', '22, rue H. Voiron', 'Montceau', '71300', 'France', '85.57.00.07');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (28, 'Gai pÃ¢turage', 'Eliane Noz', 'Bat. B 3, rue des Alpes', 'Annecy', '74000', 'France', '38.76.98.06');
INSERT INTO `SUPPLIER` (`id`, `companyname`, `contactname`, `address`, `city`, `postalcode`, `country`, `phone`) VALUES (29, 'ForÃªts d\\\'Ã©rables', 'Chantal Goulet', '148 rue Chasseur', 'Ste-Hyacinthe', 'J2S 7S8', 'Canada', '(514) 555-2955');

COMMIT;

