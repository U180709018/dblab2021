use company;

select * from customers;

Update customers set country=replace(country, '\n', '');
Update customers set city=replace(city, '\n', '');

create view mexicanCustomers as
select customerid, customername, contactname
from customers
where country="Mexico";

select * from mexicancustomers;


select *
from mexicancustomers join orders on mexicancustomers.customerid = orders.customerid;

create view productsbelowavg as
select productid, productname, price
from products
where price < (select avg(price) from products);

delete from orderdetails where productID=5;
truncate orderdetails;

delete from costumers;
delete from orders;


drop table customers;











